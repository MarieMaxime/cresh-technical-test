import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError, map, Observable, of, throwError} from "rxjs";
import {Transaction} from "../Models/models";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiUrl = 'http://localhost:3000/';
  error = false

  constructor(private http: HttpClient) {
  }

  getTransactions(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(this.apiUrl + "transactions")
      .pipe(
        map((result: Transaction[]) => {
          return result;
        }), catchError(this.handleError("getTransactions", []))
      )
  }

  private handleError<T>(operation: string, result?: T) {
    return (error: HttpErrorResponse): Observable<T> => {
      console.log(operation + ' failed !');
      console.error(error);
      if (result) {
        // Let the app keep running by returning an empty result.
        this.error = true;
        return of(result as T);
      } else {
        // or throw error
        this.error = true;
        return throwError(() => error);
      }
    };
  }
}
