import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-loading-tab-content',
  templateUrl: './loading-tab-content.component.html',
  styleUrls: ['./loading-tab-content.component.scss']
})
export class LoadingTabContentComponent {

  @Input() public displayOngoingLoader = false;

}
