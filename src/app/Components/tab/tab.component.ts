import {Component, Input} from '@angular/core';
import {Transaction} from "../../Models/models";

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent {

  @Input('tabTitle') title: string | undefined;
  @Input() active = false;
  @Input() transactions: Transaction[] | undefined;

}
