import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../Services/api.service";
import {Instalment, Transaction} from "../../Models/models";

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  ongoing: Transaction[] = [];
  history: Transaction[] = [];
  historyAllTimePaid: number = 0;
  ongoingDue: number = 0;
  loading = true;
  error = false;

  constructor(private api: ApiService) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.api.getTransactions().subscribe({
      next: this.handleGetTransactionsResponse.bind(this),
      error: this.handleGetTransactionsError.bind(this)
    });
  };

  handleGetTransactionsResponse(resp: Transaction[])  {
    for (let i = 0; i < resp.length; i++) {
      if (resp[i].is_completed) {
        this.history.push(resp[i]);
        this.historyAllTimePaid += resp[i].amount;
      } else {
        this.ongoing.push(resp[i]);
        this.ongoingDue += 1;
      }
      for (let j = 0; j < resp[i].instalments.length; j++) {
        if (!resp[i].instalments[j].is_paid) {
          this.ongoingDue += resp[i].instalments[j].amount;
        }
      }
    }
    this.loading = false;
    this.error = this.api.error;
  };

  handleGetTransactionsError() {
    console.error('cannot get transactions');
  };

  getNextInstalment(instalments: Instalment[]) {
    const filteredArray = instalments.filter(obj => !obj.is_paid)
    const minDate: Date | null = new Date(
      Math.min(
        ...filteredArray.map(({date}) => {
            return new Date(date).getTime()
        }),
      ),
    );
    return minDate
  }

  getLastPayment(instalments: Instalment[]) {
    const maxDate: Date | null = new Date(
      Math.min(
        ...instalments.map(({date}) => {
          return new Date(date).getTime()
        }),
      ),
    );
    return maxDate
  }

  calculateOngoingProgression(ongoingItem: Transaction) {
    const test = ongoingItem.instalments.filter(obj => obj.is_paid);
    const value = (test.length / ongoingItem.split)*100;
    return value + "%"
  }

}
