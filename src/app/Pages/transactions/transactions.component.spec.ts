import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsComponent } from './transactions.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {TabComponent} from "../../Components/tab/tab.component";
import {TabsComponent} from "../../Components/tabs/tabs.component";
import {apiMock} from "../../Models/apiMock";
import {RouterTestingModule} from "@angular/router/testing";

describe('TransactionsComponent', () => {
  let component: TransactionsComponent;
  let fixture: ComponentFixture<TransactionsComponent>;

  const instalmentsMock = [
    {"date": "2020-02-10T13:41:34+0100", "amount": 13500, "is_paid": true },
    {"date": "2020-03-10T22:00:00+0100", "amount": 13500, "is_paid": true },
    {"date": "2020-04-10T22:00:00+0100", "amount": 13500, "is_paid": false },
    {"date": "2020-05-10T22:00:00+0100", "amount": 13500, "is_paid": false }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        TransactionsComponent,
        TabComponent,
        TabsComponent
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get next instalment date', () => {
    expect(component.getNextInstalment(instalmentsMock)).toEqual(new Date("2020-04-10T22:00:00+0100"));
  });

  it('should get last payment date', () => {
    expect(component.getLastPayment(instalmentsMock)).toEqual(new Date("2020-02-10T13:41:34+0100"));
  });

  it('should toggle loading property',  () => {
    expect(component.loading).toBeTruthy();
    component.handleGetTransactionsResponse(apiMock);
    expect(component.loading).toBeFalsy();
  });

});
