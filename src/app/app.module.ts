import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TransactionsComponent} from './Pages/transactions/transactions.component';
import {HttpClientModule} from "@angular/common/http";
import { TabComponent } from './Components/tab/tab.component';
import { TabsComponent } from './Components/tabs/tabs.component';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LoadingTabContentComponent } from './Components/loading-tab-content/loading-tab-content.component';
import { ErrorTabContentComponent } from './Components/error-tab-content/error-tab-content.component';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    TransactionsComponent,
    TabComponent,
    TabsComponent,
    LoadingTabContentComponent,
    ErrorTabContentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
