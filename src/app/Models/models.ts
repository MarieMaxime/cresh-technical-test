export interface Transaction {
  id: number
  amount: number
  store_name: string
  split: number
  is_completed: boolean
  instalments: Instalment[]
  created_date: string
}

export interface Instalment {
  date: string
  amount: number
  is_paid: boolean
}
