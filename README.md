# Test technique Cresh par Maxime MARIE

## Présentation

Ce projet est réalisé avec Angular 13 sans librairie supplémentaire

## UX/UI

Design réalisé sur [Figma](https://www.figma.com/file/eETsnHh7esSQavCs7akNut/Test-technique-Cresh?node-id=0%3A1) (lien du projet)

## Schema de structure du projet
![structure](src/assets/structure.png)

## Responsive

Le développement est fait au format Iphone 12/13 mini, mais s'adapte également au format tablette 

## Lancer le projet

    npm i
    
    ng serve

## Lancer les tests
   
    ng test
